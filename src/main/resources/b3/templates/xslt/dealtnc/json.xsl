<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" omit-xml-declaration="yes" indent="no" encoding="UTF-8"/>

	<xsl:output method="text"/>

	<xsl:template match="/">
		<xsl:text>{"table": [</xsl:text>
		<xsl:variable name="firstColNotEmptyValues" select="/HTML/BODY/DIV/TABLE/TBODY/TR/TD[1][translate(normalize-space(.), ' ', '') != '']"/>
		<xsl:variable name="columns" select="/HTML/BODY/DIV/TABLE/TBODY/TR[1]/TD"/>
		<xsl:variable name="lines" select="/HTML/BODY/DIV/TABLE/TBODY/TR"/>
		<xsl:variable name="linesCount" select="count($lines)"/>

		<xsl:for-each select="$firstColNotEmptyValues">
			<xsl:variable name="i" select="position()"/>
			<xsl:variable name="isLast" select="$i = last()"/>

			<!--printing lines before-->
			<xsl:if test="$i != 1">
				<xsl:text>{"line": {</xsl:text>
				<xsl:for-each select="$columns">
					<xsl:variable name="k" select="position()"/>
					<xsl:variable name="colVal">
						<xsl:call-template name="printJoinedColumns">
							<xsl:with-param name="colIndex" select="$k"/>
							<xsl:with-param name="from"
							                select="count(/HTML/BODY/DIV/TABLE/TBODY/TR[TD[1] = $firstColNotEmptyValues[$i - 1]]/preceding-sibling::*)"/>
							<xsl:with-param name="to"
							                select="count(/HTML/BODY/DIV/TABLE/TBODY/TR[TD[1] = $firstColNotEmptyValues[$i]]/preceding-sibling::*) + 1"/>
							<xsl:with-param name="lines" select="$lines"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:if test="$k != 1">,</xsl:if>"column<xsl:value-of select="$k"/>": "<xsl:value-of select="$colVal"/>"
				</xsl:for-each>
				<xsl:text>}}</xsl:text>,
			</xsl:if>

			<!--printing last line-->
			<xsl:if test="$isLast">
				<xsl:text>{"line": {</xsl:text>
				<xsl:for-each select="$columns">
					<xsl:variable name="k" select="position()"/>
					<xsl:variable name="colVal">
						<xsl:call-template name="printJoinedColumns">
							<xsl:with-param name="colIndex" select="$k"/>
							<xsl:with-param name="from"
							                select="count(/HTML/BODY/DIV/TABLE/TBODY/TR[TD[1] = $firstColNotEmptyValues[$i]]/preceding-sibling::*)"/>
							<xsl:with-param name="to" select="$linesCount + 1"/>
							<xsl:with-param name="lines" select="$lines"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:if test="$k != 1">,</xsl:if>"column<xsl:value-of select="$k"/>": "<xsl:value-of select="$colVal"/>"
				</xsl:for-each>
				<xsl:text>}}</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]}</xsl:text>
	</xsl:template>

	<xsl:template name="printJoinedColumns">
		<xsl:param name="colIndex"/>
		<xsl:param name="from"/>
		<xsl:param name="to"/>
		<xsl:param name="lines"/>
		<xsl:for-each select="$lines">
			<xsl:variable name="lineIndex" select="position()"/>
			<xsl:variable name="colVal" select="/HTML/BODY/DIV/TABLE/TBODY/TR[$lineIndex]/TD[$colIndex]"/>
			<xsl:variable name="isInRangeAndNotEmpty" select="$from &lt; $lineIndex and $to &gt; $lineIndex and translate(normalize-space($colVal), ' ', '') != ''"/>
			<xsl:variable name="isNotFirst" select="not($lineIndex = $from + 1)"/>

			<xsl:if test="$isInRangeAndNotEmpty">
				<xsl:variable name="result">
					<xsl:call-template name="string-replace-all">
						<xsl:with-param name="text" select="/HTML/BODY/DIV/TABLE/TBODY/TR[$lineIndex]/TD[$colIndex]"/>
						<xsl:with-param name="replace" select="'&quot;'"/>
						<xsl:with-param name="by" select="'\&quot;'"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test="$result != ''">
					<xsl:if test="$isNotFirst"><xsl:text> </xsl:text></xsl:if>
					<xsl:value-of select="translate(normalize-space($result), ' ', '')"/>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="string-replace-all">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="by"/>
		<xsl:choose>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text,$replace)"/>
				<xsl:value-of select="$by"/>
				<xsl:call-template name="string-replace-all">
					<xsl:with-param name="text" select="substring-after($text, $replace)"/>
					<xsl:with-param name="replace" select="$replace"/>
					<xsl:with-param name="by" select="$by"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>