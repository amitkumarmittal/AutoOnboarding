<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="UTF-8"/>

    <xsl:template match="/">
        <html>
            <head><title>Terms and Conditiions</title></head>
            <body>
                <table border="1">
                    <xsl:for-each select="HTML/BODY/DIV/TABLE/TBODY/TR">
                        <tr>
                            <xsl:for-each select="TD">
                                <xsl:choose>
                                    <xsl:when test="STRONG">
                                        <td><xsl:value-of select="STRONG"/></td>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <td><xsl:value-of select="normalize-space(text())"/></td>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>