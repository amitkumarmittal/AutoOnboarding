package com.sita.aero.model;

import java.io.Serializable;

public class RowRecord implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5220449399154405092L;
	String sheetName;
	
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
}
