package com.sita.aero.model;

import java.io.Serializable;

public class DefaultRowRecord extends RowRecord implements Serializable{
	
	private static final long serialVersionUID = 2402339593388150647L;
	String type;
	String id;
	String validValues;
	String sitaDefaults;
	String airlineRequestedValues;
	String phase;
	String comments;
	String option;
	
	
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValidValues() {
		return validValues;
	}
	public void setValidValues(String validValues) {
		this.validValues = validValues;
	}
	public String getSitaDefaults() {
		return sitaDefaults;
	}
	public void setSitaDefaults(String sitaDefaults) {
		this.sitaDefaults = sitaDefaults;
	}
	public String getAirlineRequestedValues() {
		return airlineRequestedValues;
	}
	public void setAirlineRequestedValues(String airlineRequestedValues) {
		this.airlineRequestedValues = airlineRequestedValues;
	}
	public String getPhase() {
		return phase;
	}
	public void setPhase(String phase) {
		this.phase = phase;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((option == null) ? 0 : option.hashCode());
		result = prime * result + ((airlineRequestedValues == null) ? 0 : airlineRequestedValues.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((sitaDefaults == null) ? 0 : sitaDefaults.hashCode());
		result = prime * result + ((validValues == null) ? 0 : validValues.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultRowRecord other = (DefaultRowRecord) obj;
		if (option == null) {
			if (other.option != null)
				return false;
		} else if (!option.equals(other.option))
			return false;
		if (airlineRequestedValues == null) {
			if (other.airlineRequestedValues != null)
				return false;
		} else if (!airlineRequestedValues.equals(other.airlineRequestedValues))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (sitaDefaults == null) {
			if (other.sitaDefaults != null)
				return false;
		} else if (!sitaDefaults.equals(other.sitaDefaults))
			return false;
		if (validValues == null) {
			if (other.validValues != null)
				return false;
		} else if (!validValues.equals(other.validValues))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DefaultRowRecord [type=" + type + ", id=" + id + ", validValues=" + validValues + ", sitaDefaults="
				+ sitaDefaults + ", airlineRequestedValues=" + airlineRequestedValues + ", phase=" + phase
				+ ", comments=" + comments + ", sheetName=" + sheetName + ", option=" + option + "]";
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
