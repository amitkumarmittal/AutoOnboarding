package com.sita.aero.model;

import java.io.Serializable;

public class OriginDestPair extends RowRecord implements Serializable{

	private static final long serialVersionUID = -5028273168652317938L;
	
	private String originCity;
	private String destCity;
	private String originAirportCode;
	private String destAirportCode;
	private String originCountry;
	private String destCountry;
	private String originAirportName;
	private String destAirportName;
	private Boolean returnFlightAvailable = false;
	
	/**
	 * @return the returnFlightAvailable
	 */
	public Boolean getReturnFlightAvailable() {
		return returnFlightAvailable;
	}
	/**
	 * @param returnFlightAvailable the returnFlightAvailable to set
	 */
	public void setReturnFlightAvailable(Boolean returnFlightAvailable) {
		this.returnFlightAvailable = returnFlightAvailable;
	}
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public String getDestCity() {
		return destCity;
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public String getOriginAirportCode() {
		return originAirportCode;
	}
	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}
	public String getDestAirportCode() {
		return destAirportCode;
	}
	public void setDestAirportCode(String destAirportCode) {
		this.destAirportCode = destAirportCode;
	}
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	public String getDestCountry() {
		return destCountry;
	}
	public void setDestCountry(String destCountry) {
		this.destCountry = destCountry;
	}
	public String getOriginAirportName() {
		return originAirportName;
	}
	public void setOriginAirportName(String originAirportName) {
		this.originAirportName = originAirportName;
	}
	public String getDestAirportName() {
		return destAirportName;
	}
	public void setDestAirportName(String destAirportName) {
		this.destAirportName = destAirportName;
	}
	@Override
	public String toString() {
		return "[" + originAirportCode + "-" + destAirportCode + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destAirportCode == null) ? 0 : destAirportCode.hashCode());
		result = prime * result + ((destCity == null) ? 0 : destCity.hashCode());
		result = prime * result + ((destCountry == null) ? 0 : destCountry.hashCode());
		result = prime * result + ((originAirportCode == null) ? 0 : originAirportCode.hashCode());
		result = prime * result + ((originCity == null) ? 0 : originCity.hashCode());
		result = prime * result + ((originCountry == null) ? 0 : originCountry.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OriginDestPair other = (OriginDestPair) obj;
		if (destAirportCode == null) {
			if (other.destAirportCode != null)
				return false;
		} else if (!destAirportCode.equals(other.destAirportCode))
			return false;
		if (destCity == null) {
			if (other.destCity != null)
				return false;
		} else if (!destCity.equals(other.destCity))
			return false;
		if (destCountry == null) {
			if (other.destCountry != null)
				return false;
		} else if (!destCountry.equals(other.destCountry))
			return false;
		if (originAirportCode == null) {
			if (other.originAirportCode != null)
				return false;
		} else if (!originAirportCode.equals(other.originAirportCode))
			return false;
		if (originCity == null) {
			if (other.originCity != null)
				return false;
		} else if (!originCity.equals(other.originCity))
			return false;
		if (originCountry == null) {
			if (other.originCountry != null)
				return false;
		} else if (!originCountry.equals(other.originCountry))
			return false;
		return true;
	}
	
	
	
}
