package com.sita.aero.model;

import java.io.Serializable;


public class SsrRecord implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -235973683310819952L;
	String ssrCode;
	String ssrDescription;
	String ssrRules;
	String individualSsr;
	/**
	 * @return the ssrCode
	 */
	public String getSsrCode() {
		return ssrCode;
	}
	/**
	 * @param ssrCode the ssrCode to set
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}
	/**
	 * @return the ssrDescription
	 */
	public String getSsrDescription() {
		return ssrDescription;
	}
	/**
	 * @param ssrDescription the ssrDescription to set
	 */
	public void setSsrDescription(String ssrDescription) {
		this.ssrDescription = ssrDescription;
	}
	/**
	 * @return the ssrRules
	 */
	public String getSsrRules() {
		return ssrRules;
	}
	/**
	 * @param ssrRules the ssrRules to set
	 */
	public void setSsrRules(String ssrRules) {
		this.ssrRules = ssrRules;
	}
	/**
	 * @return the individualSsr
	 */
	public String getIndividualSsr() {
		return individualSsr;
	}
	public SsrRecord(String ssrCode, String ssrDescription, String ssrRules, String individualSsr) {
		super();
		this.ssrCode = ssrCode;
		this.ssrDescription = ssrDescription;
		this.ssrRules = ssrRules;
		this.individualSsr = individualSsr;
	}
	/**
	 * @param individualSsr the individualSsr to set
	 */
	public void setIndividualSsr(String individualSsr) {
		this.individualSsr = individualSsr;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SsrRecord [ssrCode=" + ssrCode + ", ssrDescription=" + ssrDescription + ", ssrRules=" + ssrRules
				+ ", individualSsr=" + individualSsr + "]";
	}
	
	
}
