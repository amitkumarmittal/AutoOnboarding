package com.sita.aero.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sita.aero.helper.ExcelFileReader;
import com.sita.aero.helper.FileAndDirHelper;
import com.sita.aero.helper.SsrValueHelper;
import com.sita.aero.model.DefaultRowRecord;
import com.sita.aero.model.OriginDestPair;
import com.sita.aero.model.RowRecord;
import com.sita.aero.model.SsrRecord;

@Controller
public class UploadController {

	private static final Logger logger = LoggerFactory.getLogger("UploadController");

	//private static final DefaultRowRecord[][] DefaultRowRecord = null;

    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "/temp";
    
    @Autowired
    private ExcelFileReader fileReader;
    
    @Autowired
    private SsrValueHelper ssrValueHelper;
    
    @Autowired
    private FileAndDirHelper fileHelper;

    @GetMapping("/")
    public String index() {
        return "upload";
    }

    
    @PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
    	String redirect = "redirect:uploadStatus";
    	if (file.isEmpty()) {
        	logger.info("File empty");
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return redirect;
        }

        try {
        	// Get the file and save it somewhere
			byte[] bytes = file.getBytes();
			String filename = file.getOriginalFilename();
			Path path = Paths.get(UPLOADED_FOLDER + filename);
			Files.write(path, bytes);
			logger.info("Successfully Uploaded file in temp folder");
            logger.info("Trying to parse the file");
            List<RowRecord> allWorkBookRecords = fileReader.read(UPLOADED_FOLDER, filename);
            
            redirect = readAndCreateData(redirectAttributes, filename, allWorkBookRecords);
            
        } catch (IOException e) {
            logger.error("Error while writing to file", e);
        }

        return "redirect:/uploadStatus";
    }

    @GetMapping("/read") // //new annotation since 4.3
    public String fileRead(RedirectAttributes redirectAttributes) {
    		String filename = "temp.xlsx";
        
            logger.info("Successfully Uploaded file in temp folder");
            logger.info("Trying to parse the file");
            List<RowRecord> allWorkBookRecords = fileReader.read(UPLOADED_FOLDER, "/"+filename);
            
            return readAndCreateData(redirectAttributes, filename, allWorkBookRecords);
    }


	private String readAndCreateData(RedirectAttributes redirectAttributes, String filename,
			List<RowRecord> allWorkBookRecords) {
		Map<String, DefaultRowRecord> idValueMap = fileReader.getIdValueMap(allWorkBookRecords);
		
		Properties props = fileHelper.getProperties("/b3/config.properties");
		String airline = idValueMap.get("1.1").getAirlineRequestedValues();
		String airlineFolder = UPLOADED_FOLDER +"/"+ airline;
		fileHelper.copyFolder(fileHelper.getFileFromURL("b3"), new File(UPLOADED_FOLDER +"/"+ airline));
		
		List<OriginDestPair> odRecords = allWorkBookRecords.stream()
														   .filter(record -> record instanceof OriginDestPair)
														   .map(record -> (OriginDestPair) record)
														   .collect(Collectors.toList());
		
		fileReader.createOriginDestFile(odRecords, airlineFolder + "/OnD.csv");
		props.setProperty("AIRLINE_CODE", airline);
		props.setProperty("DEFAULT_AIRPORT", idValueMap.get("5.4").getAirlineRequestedValues());
		props.setProperty("DEFAULT_CURRENCY", idValueMap.get("8.6").getAirlineRequestedValues());
		props.setProperty("LIST_CABIN_CLASSES", idValueMap.get("5.6").getAirlineRequestedValues());
		props.setProperty("AIRFARE_DEPT_CODE", idValueMap.get("2.2").getAirlineRequestedValues());
		//MAP_TTL_VALUE to be populated 
		//pax types to be populated
		//to be evaluated from Airport
		//props.setProperty("BOOKING_OFFICE_TIMEZONE", idValueMap.get("5.4").getAirlineRequestedValues());
		props.setProperty("ADVANCE_PURCHASE_WINDOW", idValueMap.get("8.1").getAirlineRequestedValues());
		props.setProperty("DAYS_TO_MAX_DEPT_DATE", idValueMap.get("8.1").getAirlineRequestedValues());
		props.setProperty("companyDetails.ADDRESS_TRANSLATION_MAP_ADDRESS", idValueMap.get("3.2").getAirlineRequestedValues());

		
		List<SsrRecord> ssrBeanElement = ssrValueHelper.getSSRElements(idValueMap.get("18.2"));
		String ssrs = ssrBeanElement.stream()
									.map(record->record.getSsrCode())
									.reduce((record, ssr) -> record  +","+ ssr)
									.orElse("");
		
		props.setProperty("SSR_LIST_PRIORITY_TABLE", ssrs);
		
		ssrBeanElement.addAll(ssrValueHelper.getSSRElements(idValueMap.get("16.2")));
		
		logger.info("SSR Bean Elements start--------");
		
		try {
			ssrValueHelper.replaceSSRElementInMainFile(ssrBeanElement, airlineFolder + "/_main.config.xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("SSR Bean Elements Ends--------");
		
		logger.info("File read successful");
		
		ssrValueHelper.finalisePaxTypeXML(idValueMap.get("6.1"), airlineFolder +"/paxtypes.config.xml");
		
		
		fileHelper.writeToProperties(UPLOADED_FOLDER ,  airline + "/config.properties", props);
		
		
		
		redirectAttributes.addFlashAttribute("message",
		        "You successfully read '" + filename + "'");
		redirectAttributes.addFlashAttribute("workbook", allWorkBookRecords);
		
      

      return "redirect:/uploadStatus";
	}

	

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }

} 