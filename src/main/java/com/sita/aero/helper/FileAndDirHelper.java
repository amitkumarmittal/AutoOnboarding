package com.sita.aero.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.sita.aero.model.Airport;

@Component
public class FileAndDirHelper {
	/*
	 * Logger for the file
	 */
	private static final Logger logger = LoggerFactory.getLogger("FileAndDirHelper");

	public Properties getProperties(String fileName) {
		Properties prop = new Properties();
		try (InputStream input = this.getClass().getResourceAsStream(fileName)) {
			prop.load(input);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return prop;
	}

	public void writeToProperties(String folder, String filename, Properties prop) {
		createFolderIfNotExist(folder);
		try (FileOutputStream output = new FileOutputStream(folder + "/" + filename)) {
			prop.store(output, null);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	private void createFolderIfNotExist(String folder) {
		File files = new File(folder);
		if (!files.exists()) {
			if (files.mkdirs()) {
				logger.info("sub directories created successfully");
			} else {
				logger.info("failed to create sub directories");
			}
		}

	}

	public void copyFolder(File src, File dest) {
		if (src.exists()) {
			if (src.isDirectory()) {
				// if directory not exists, create it
				if (!dest.exists()) {
					dest.mkdir();
				}

				// list all the directory contents
				String files[] = src.list();

				for (String file : files) {
					// construct the src and dest file structure
					File srcFile = new File(src, file);
					File destFile = new File(dest, file);
					// recursive copy
					copyFolder(srcFile, destFile);
				}

			} else {
				// if file, then copy it
				// Use bytes stream to support all file types
				try (InputStream in = new FileInputStream(src); OutputStream out = new FileOutputStream(dest);) {
					byte[] buffer = new byte[1024];

					int length;
					// copy the file content in bytes
					while ((length = in.read(buffer)) > 0) {
						out.write(buffer, 0, length);
					}
					in.close();
					out.close();
					logger.info("File copied from " + src + " to " + dest);
				} catch (IOException ioe) {
					logger.info("Error while copying file", ioe);
				}
			}
		}
	}
	
	public File getFileFromURL(String src) {
	    URL url = this.getClass().getClassLoader().getResource(src);
	    File file = null;
	    try {
	        file = new File(url.toURI());
	    } catch (URISyntaxException e) {
	        file = new File(url.getPath());
	    } 
	    return file;
	}

	public void replaceFileString(File file, String old, String changeString) throws IOException {
		Path path = file.toPath();
		Charset charset = StandardCharsets.UTF_8;

		String content = new String(Files.readAllBytes(path), charset);
		content = content.replaceAll(old, changeString);
		Files.write(path, content.getBytes(charset));
	}
	
	
	public Map<String, Airport> readAirportData(){
		String csvFile = "airports.csv";
        String line = "";
        String cvsSplitBy = ",";
        Map<String, Airport> airports = new HashMap<String, Airport>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] country = line.split(cvsSplitBy);
                Airport airport = new Airport();
                airport.setName(country[0]);
                airport.setCity(country[1]);
                airport.setCountry(country[2]);
                airport.setCode(country[3]);
                airport.setTimezone(country[4]);
                airport.setCountryCode(country[5]);
                airports.put(country[3], airport);
                //System.out.println("Country [code= " + country[4] + " , name=" + country[5] + "]");

            }

        } catch (IOException e) {
            logger.info("Error reading airports", e);
        }
        return airports;
	}
	
	public void uncommentRequiredPaxtypeInXML(String value, File file){
		Path path = file.toPath();
		Charset charset = StandardCharsets.UTF_8;
		String valueStr = value.replaceAll(" ", "");
		StringBuilder content;
		try {
			content = new StringBuilder(new String(Files.readAllBytes(path), charset));
			int firstIndex = content.indexOf("<!--<ref bean=\"PassengerType_"+ valueStr);
			content.replace(firstIndex, firstIndex+5, "<");
			
			int nextIndex = content.indexOf(">-->", firstIndex);
			content.replace(nextIndex, nextIndex+4, ">");
			
			firstIndex = content.indexOf("<!--<bean id=\"PassengerType_"+ valueStr);
			content.replace(firstIndex, firstIndex+5, "<");
			
			nextIndex = content.indexOf(">-->", firstIndex);
			content.replace(nextIndex, nextIndex+4, ">");
			
			Files.write(path, content.toString().getBytes(charset));
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
}