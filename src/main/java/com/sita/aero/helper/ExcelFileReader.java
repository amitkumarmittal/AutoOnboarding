package com.sita.aero.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.sita.aero.model.DefaultRowRecord;
import com.sita.aero.model.OriginDestPair;
import com.sita.aero.model.RowRecord;

/**
 * @author Amit.1.mittal
 *Class to read the excel file and convert them in row records formats.
 */
@Component
public class ExcelFileReader {

	/**
	 * Origin destination sheet name
	 */
	public static final String ORIGIN_DEST = "O&Ds";
	/**
	 * check in routes sheet name
	 */
	private static final String CHECKIN_ROUTES = "Check In Routes";
	/**
	 * Fare family sheet name.
	 */
	private static final String FARE_FAMILY = "Fare Family";
	/**
	 * constant Array for non default sheets names.
	 */
	public static final List<String> NotDefaultSheets = Arrays.asList("SITA", ORIGIN_DEST, FARE_FAMILY,
			CHECKIN_ROUTES);

	/*
	 * private static final List<String> defaultSheets = Arrays.asList(
	 * " Branding and Set-up", "Payments", "Confirmation Page & Email",
	 * " Passenger Information", " Search and Shop", "Web Check In Data",
	 * "MyBookingManager", "Internal Config (SITA ONLY)");
	 */
	// private static final String FILE_NAME = "/tmp/MyFirstExcel.xlsx";
	/**
	 * logger
	 */
	private static final Logger logger = LoggerFactory.getLogger("ExcelFileReader");

	/**
	 * Method to read workbook and convert to row records.
	 * @param uploadedFile file to upload
	 * @return collection of records.
	 */
	public List<RowRecord> read(String folderName, String uploadedFile) {
		List<RowRecord> allWorkBookRecords = new ArrayList<>();
		try (FileInputStream excelFile = new FileInputStream(folderName + uploadedFile);
				Workbook workbook = new XSSFWorkbook(excelFile)) {
			workbook.spliterator().forEachRemaining(datatypeSheet -> {
				String sheetName = datatypeSheet.getSheetName().trim();
				if (NotDefaultSheets.contains(sheetName)) {
					if (sheetName.equals(ORIGIN_DEST)) {
						allWorkBookRecords.addAll(processOriginDestSheet(datatypeSheet));
					} 
					/*else if (sheetName.equals(FARE_FAMILY)) {
						allWorkBookRecords.addAll(processFareFamilySheet(datatypeSheet));
					}*/
				}
				else {
						allWorkBookRecords.addAll(processDefaultSheet(datatypeSheet, sheetName));
					}
				
			});
			logger.info(allWorkBookRecords.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return allWorkBookRecords;
	}


	/**
	 * Process origin destination sheet
	 * @param datatypeSheet sheet object
	 * @return list of origin destination pairs
	 */
	private List<? extends RowRecord> processOriginDestSheet(Sheet datatypeSheet) {
		// TODO Auto-generated method stub
		List<OriginDestPair> originDestRecords = new ArrayList<>();
		OriginDestPair prevRecord = null;
		//String csvFile = "OnD.csv";
		
			// FileWriter writer = new FileWriter(csvFile);
			for (Row currentRow : datatypeSheet) {
				OriginDestPair record = new OriginDestPair();
				record.setSheetName("OnDs");
				if (currentRow.getRowNum() > 0) {
					logger.info("" + currentRow.getRowNum());
					getOriginDestObj(currentRow, record);
					if ((record.getOriginCity() != null && !record.getOriginCity().equals(""))) {
						if (prevRecord != null && prevRecord.getOriginAirportCode().equals(record.getDestAirportCode())
								&& prevRecord.getDestAirportCode().equals(record.getOriginAirportCode())) {
							prevRecord.setReturnFlightAvailable(true);
						} else {
							originDestRecords.add(record);
						}
					}
					prevRecord = record;
				}
				
			}
		
		return originDestRecords;
		
	}
	
	public void createOriginDestFile(List<OriginDestPair> originDestRecords, String csvFile){
		
		try (FileWriter writer = new FileWriter(csvFile)) {
			originDestRecords.forEach(record -> {
				List<String> pairlist = createODCsv(record);
				try {
				ExcelFileWriter.writeLine(writer, pairlist, ',', ' ');
				}catch (IOException e) {
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private List<String> createODCsv(OriginDestPair record) {
		
							List<String> pairList=new ArrayList<String>();
							  
							  OriginDestPair pair = (OriginDestPair) record;
							  StringBuilder builder = new StringBuilder("(");
							  
							  builder.append(pair.getOriginAirportCode());
							  builder.append(")");
							  pairList.add(builder.toString());
							  
							  builder.delete(0, builder.length());
							  
							  builder.append("(");
							  builder.append(pair.getDestAirportCode());
							  builder.append(")");
							  pairList.add(builder.toString());
							  
							  pairList.add(pair.getReturnFlightAvailable().toString());
							  
							  
								return pairList;
	}

	/**
	 * Process default type sheets
	 * @param datatypeSheet sheet object
	 * @param sheetName sheet name string
	 * @return list of default row records
	 */
	private List<? extends RowRecord> processDefaultSheet(Sheet datatypeSheet, 
					String sheetName) {
		//String sheetName = datatypeSheet.getSheetName();
		List<DefaultRowRecord> defaultWorkBookRecords =  new ArrayList<>(); 
		logger.info(sheetName);
		String type = "";
		for (Row currentRow : datatypeSheet) {
			DefaultRowRecord record = new DefaultRowRecord();
			record.setSheetName(sheetName);
			if (currentRow.getRowNum() > 0) {
				logger.info("" + currentRow.getRowNum());
				getDefaultRecordObj(currentRow, record);
				if (record.getId() != null && !record.getId().equals("")) {
					if (record.getId().endsWith(".0") && (record.getAirlineRequestedValues() == null
							|| record.getAirlineRequestedValues().equals(""))) {
						logger.info(record.getOption());
						type = record.getOption();
					} else {
						record.setType(type);
						defaultWorkBookRecords.add(record);
					}
				}
			}
		}
		return defaultWorkBookRecords;
	}

	/**
	 * Update rocord object from row data
	 * @param currentRow Row 
	 * @param record Record to update
	 */
	private void getDefaultRecordObj(Row currentRow, DefaultRowRecord record) {
		Iterator<Cell> cellIterator = currentRow.iterator();
		while (cellIterator.hasNext()) {
			Cell currentCell = cellIterator.next();
			switch (currentCell.getAddress().getColumn()) {
			case 0:
				record.setId(getCellString(currentCell));
				break;
			case 1:
				record.setOption(getCellString(currentCell));
				break;
			case 2:
				record.setValidValues(getCellString(currentCell));
				break;
			case 3:
				record.setSitaDefaults(getCellString(currentCell));
				break;
			case 4:
				record.setAirlineRequestedValues(getCellString(currentCell));
				break;
			case 5:
				record.setPhase(getCellString(currentCell));
				break;
			case 6:
				record.setComments(getCellString(currentCell));
				break;
			}
		}
	}
	
	/**
	 * Method to updated Origin dest record with actual data
	 * @param currentRow row to parse
	 * @param record Rocord to update
	 */
	private void getOriginDestObj(Row currentRow, OriginDestPair record) {
		Iterator<Cell> cellIterator = currentRow.iterator();
		while (cellIterator.hasNext()) {
			Cell currentCell = cellIterator.next();
			switch (currentCell.getAddress().getColumn()) {
			case 0:
				record.setOriginCity(getCellString(currentCell));
				break;
			case 1:
				record.setOriginAirportCode(getCellString(currentCell));
				break;
			case 2:
				record.setOriginAirportName(getCellString(currentCell));
				break;
			case 3:
				record.setOriginCountry(getCellString(currentCell));
				break;
			case 4:
				record.setDestCity(getCellString(currentCell));
				break;
			case 5:
				record.setDestAirportCode(getCellString(currentCell));
				break;
			case 6:
				record.setDestAirportName(getCellString(currentCell));
				break;
			case 7:
				record.setDestCountry(getCellString(currentCell));
				break;
			}
		}
	}


	/**
	 * Method to get cell string
	 * @param currentCell cell
	 * @return String value
	 */
	@SuppressWarnings("deprecation")
	private String getCellString(Cell currentCell) {
		if (currentCell.getCellTypeEnum() == CellType.STRING) {
			return currentCell.getStringCellValue().trim();
		} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
			return String.valueOf(currentCell.getNumericCellValue());
		} else
			return "";
	}
	
	
	public Map<String, DefaultRowRecord> getIdValueMap(List<RowRecord> allWorkBookRecords){
		Map<String, DefaultRowRecord> idValueMap = 
				allWorkBookRecords.stream()
								  .filter(record -> record instanceof DefaultRowRecord)
								  .map(record -> (DefaultRowRecord) record)
								  .collect(Collectors.toMap(record -> record.getId(), 
										   			record -> record));
		return idValueMap;
	}
}