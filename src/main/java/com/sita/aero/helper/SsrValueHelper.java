package com.sita.aero.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sita.aero.model.DefaultRowRecord;
import com.sita.aero.model.SsrRecord;

@Component
public class SsrValueHelper {
	
	@Autowired
	FileAndDirHelper fileHelper;
	
	public List<SsrRecord> getSSRElements(DefaultRowRecord sSRRecord) {
		List<SsrRecord> ssrRecords =  new ArrayList<>();
				String  ssrValues = sSRRecord.getAirlineRequestedValues();
				String[] ssrs= ssrValues.split("\n");
				
				for(int i=0;i<ssrs.length;i++){
					if(ssrs[i].length()>4)
						ssrRecords.add(new SsrRecord(ssrs[i].substring(0, 4),
										   ssrs[i].substring(ssrs[i].indexOf('-')+1).trim(), 
										   "true", 
										   "true"));
				}
		
		return ssrRecords;
		//logger.info(abc);
	}
	
	private String createBeanElement(List<SsrRecord> ssrRecords){
		List<String> beanList = new ArrayList<>();
		StringBuilder listBuilder = new StringBuilder("<bean id=\"ListAvailableSSRs\" class=\"java.util.ArrayList\">\n");
        listBuilder.append("\t<constructor-arg>\n");
        listBuilder.append("\t\t<list value-type=\"aero.sita.lab.hostintegration.bindings.SsrConfiguration\">\n");
        for(SsrRecord record:ssrRecords){
        	listBuilder.append("\t\t\t<ref bean=\"SSR_");
        	listBuilder.append(record.getSsrCode());
        	listBuilder.append("\"/>\n");
        	
        	StringBuilder beanBuilder = new StringBuilder("<bean id=\"SSR_");
        	beanBuilder.append(record.getSsrCode());
        	beanBuilder.append("\" class=\"aero.sita.lab.hostintegration.bindings.SsrConfiguration\">\n");
        	beanBuilder.append("\t<property name=\"ssrCode\" value=\"");
        	beanBuilder.append(record.getSsrCode());
        	beanBuilder.append("\"/>\n");
        	beanBuilder.append("\t<property name=\"ssrDescription\" value=\"");
        	beanBuilder.append(record.getSsrDescription());
        	beanBuilder.append("\"/>\n");
        	beanBuilder.append("\t<property name=\"ssrRules\" value=\"(");
        	beanBuilder.append(record.getSsrRules());
        	beanBuilder.append(")\"/>\n");
        	beanBuilder.append("\t<property name=\"individualSsr\" value=\"");
        	beanBuilder.append(record.getIndividualSsr());
        	beanBuilder.append("\"/>\n");
        	beanBuilder.append("</bean>\n");
        	beanList.add(beanBuilder.toString());
        }        
        
        listBuilder.append("\t\t</list>\n");
        listBuilder.append("\t</constructor-arg>\n");
        listBuilder.append("</bean>\n");
        beanList.forEach(listBuilder::append);
        //listBuilder.append();
        
        return listBuilder.toString(); 
	}
	
	public void replaceSSRElementInMainFile(List<SsrRecord> ssrRecords, String fileName) throws IOException{
		fileHelper.replaceFileString(new File(fileName), "<bean id=\"ListAvailableSSRs\"/>", createBeanElement(ssrRecords));
	}

	public void finalisePaxTypeXML(DefaultRowRecord defaultRowRecord, String fileName) {
		// TODO Auto-generated method stub
		File file = new File(fileName);
		List<String> ssrRecords =  Arrays.asList(defaultRowRecord.getAirlineRequestedValues().split("\n"));
		
		ssrRecords.stream().forEach(value->fileHelper.uncommentRequiredPaxtypeInXML(value.trim(), file));
	}
	
	
	
}
